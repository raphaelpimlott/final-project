
$(function() {

  $("div#menu span").click( function(index) {
	var myMaterialTexture = new THREE.ImageUtils.loadTexture( $(this).data("texture") );
	/*Change this bit to set tiling parameters
	myMaterialTexture.wrapS = myMaterialTexture.wrapT = THREE.RepeatWrapping; 
	myMaterialTexture.repeat.set( 10, 10 );*/
    var newMaterial = new THREE.MeshLambertMaterial( { map: myMaterialTexture  } );
    updateMaterial( newMaterial );
  });
  
});
